//get list name
var firstName = $('#first-name');
var lastName = $('#last-name');

var passWord = $('#password');
var conPassword = $('#conpassword');

var email = $('#email');
var phone = $('#phone');

var male = $('#male');
var female = $('#female');

var readBook = $('#read-book');
var playGame = $('#play-game');

var question = $('#question');
var levelQuestion = $('#level-question');

var submit = $("#submit");
var reset = $('#reset');

/*
  1. Mật khẩu từ 6 ~ 32 ký tự bắt đầu ký tự in hoa và ký tự đặc biệt
*/
var onlyPassword = /^([A-Z]){1}([\w_\.!@#$%^&*()]+){5,31}$/;
var onlyNumber = /^[0-9]{10,13}$/;
// var onlyEmail = /^[A-Za-z0-9_.]{6,32}@([a-zA-Z0-9]{2,12})(.[a-zA-Z]{2,12})+$/;
var onlyEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;


// init function
var Init = {
  handleResetTypeText: function(param){
    param.val('');
  },

  handleResetTypeRadioCheckbox: function(param){
    param.prop('checked', false);
  },

  handleResetSelectOption: function(param){
    param.val('0');
  },

  handleCheckNull: function (param) {
    var value = param.val()
    if (value == "" || value == null) {
      param.focus();
      return false;
    } else {
      return true;
    }
  },

  handleShowError: function (param) {
    if (this.handleCheckNull(param)) {
      param.siblings('.text-danger').addClass('d-none');
    } else {
      param.siblings('.text-danger').removeClass('d-none');
    }
  },

  handlePassword: function () {
    this.handleShowError(passWord);
    if (!onlyPassword.test(passWord.val()) && passWord.val() != "") {
      $('.password').removeClass('d-none');
    } else {
      $('.password').addClass('d-none');
    }
  },
  handleConpassword: function () {
    this.handleShowError(conPassword);
    if ((passWord.val() != conPassword.val()) && conPassword.val() != "") {
      $('.rePassword').removeClass('d-none');
    } else {
      $('.rePassword').addClass('d-none');
    }
  },
  handleEmail: function () {
    this.handleShowError(email);
    if (!onlyEmail.test(email.val()) && email.val() != "") {
      $('.email-valid').removeClass('d-none');
    } else {
      $('.email-valid').addClass('d-none');
    }
  },

  handlePhoneNumber: function () {
    this.handleShowError(phone);
    if (!onlyNumber.test(phone.val()) && phone.val() != "") {
      $('.phone-valid').removeClass('d-none');
    } else {
      $('.phone-valid').addClass('d-none');
    }
  },

  handleRadioCheckBox: function (fieldType, fieldName, classError) {
    var $radios = $(`input:${fieldType}[name=${fieldName}]`);
    if ($radios.is(':checked') === false) {
      $(`.${classError}`).removeClass('d-none');
    } else {
      $(`.${classError}`).addClass('d-none');
    }
  },

  handleSelectOption: function (idSelect, classError) {
    var value = parseInt(idSelect.val());
    if (value == 0) {
      $(`.${classError}`).removeClass('d-none');
    } else {
      $(`.${classError}`).addClass('d-none');
    }
  }
};

$(document).ready(function () {
  //call function  
  submit.on('click', function (e) {
    e.preventDefault();
    Init.handleRadioCheckBox('radio', 'gender', 'gender');
    Init.handleRadioCheckBox('checkbox', 'hobby', 'hobby');
    Init.handleSelectOption(levelQuestion, 'level');
    Init.handleShowError(question);
    Init.handlePhoneNumber();
    Init.handleEmail();
    Init.handleConpassword();
    Init.handlePassword();
    Init.handleShowError(lastName);
    Init.handleShowError(firstName);
  });

  reset.on('click', function (e) {
    e.preventDefault();
    Init.handleResetTypeText(firstName);
    Init.handleResetTypeText(lastName);
    Init.handleResetTypeText(passWord);
    Init.handleResetTypeText(conPassword);
    Init.handleResetTypeText(email);
    Init.handleResetTypeText(phone);
    Init.handleResetTypeText(question);
    Init.handleResetTypeRadioCheckbox(readBook);
    Init.handleResetTypeRadioCheckbox(playGame);
    Init.handleResetTypeRadioCheckbox(male);
    Init.handleResetTypeRadioCheckbox(female);
    Init.handleResetSelectOption(levelQuestion);
  })
});